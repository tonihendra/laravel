<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
class Guru extends Model
{
	use Uuid;
    public $incrementing = false;
	protected $table = 'ref_guru';
	protected $primaryKey = 'guru_id';
	protected $fillable = [
        'guru_id', 'guru_id_dapodik', 'sekolah_id', 'nama', 'nuptk', 'nip', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'nik', 'jenis_ptk_id', 'agama_id', 'alamat', 'rt', 'rw', 'desa_kelurahan', 'kecamatan', 'kode_pos', 'no_hp', 'email', 'photo', 'last_sync'
    ];
}
