<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Support\Facades\Blade;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    /*public function boot()
    {
        //
    }*/
	public function boot(Dispatcher $events){
		$events->listen(BuildingMenu::class, function (BuildingMenu $event) {
			$url = parse_url(url('/'));
			$event->menu->add('periode');
			$event->menu->add([
				'text' => 'Beranda',
				'url' => 'home',
				'icon'  => 'dashboard',
				'active' => ['/', 'home'],
			]);
			if($url['host'] == 'localhost'){
				$event->menu->add([
					'text'        => 'Sinkronisasi',
					'url'         => '#',
					'icon'        => 'refresh',
					'permission'  		=> 'admin',
					'submenu' => [
						[
							'text' => 'Ambil Data',
							'url'  => 'sinkronisasi/ambil-data',
							'icon' => 'download',
							'label'       => 'Online',
							'label_color' => 'success',
							'active' => ['sinkronisasi/ambil-data', 'sinkronisasi/guru', 'sinkronisasi/rombongan-belajar', 'sinkronisasi/ref-kd'],
						],
						[
							'text' => 'Kirim Data',
							'url'  => 'sinkronisasi/kirim-data',
							'icon' => 'upload',
							'label'       => 'Online',
							'label_color' => 'success',
						],
						[
							'text' => 'Kirim Nilai',
							'url'  => 'sinkronisasi/kirim-nilai',
							'icon' => 'upload',
							'label'       => 'Offline',
							'label_color' => 'danger',
						],
					],
				]);
			} else {
				$event->menu->add([
					'text'        => 'Sinkronisasi',
					'url'         => '#',
					'icon'        => 'refresh',
					'permission'  		=> 'admin',
					'submenu' => [
						[
							'text' => 'Ambil Data',
							'url'  => 'sinkronisasi/ambil-data',
							'icon' => 'download',
							'label'       => 'Online',
							'label_color' => 'success',
							'active' => ['sinkronisasi/ambil-data', 'sinkronisasi/guru', 'sinkronisasi/rombongan-belajar', 'sinkronisasi/ref-kd'],
						],
						[
							'text' => 'Kirim Data',
							'url'  => 'sinkronisasi/kirim-data',
							'icon' => 'upload',
							'label'       => 'Online',
							'label_color' => 'success',
						],
					],
				]);
			}
			$event->menu->add([
				'text' => 'Konfigurasi',
				'url'  => '#',
				'icon'  => 'wrench',
				'permission'  => 'admin',
				'submenu' => [
					[
						'text' => 'Konfigurasi Umum',
						'url'  => 'konfigurasi',
						'icon' => 'exchange',
					],
					[
						'text' => 'Hak Akses Pengguna',
						'url'  => 'users',
						'icon' => 'user',
						'active' => ['users', 'users/edit/*'],
					],
				],
			]);
			$event->menu->add([
				'text'	=> 'Referensi',
				'url'  => '#',
				'icon' => 'list',
				'permission'  => ['admin', 'guru'],
				'submenu' => [
					[
						'text' => 'Referensi GTK',
						'url'  => '#',
						'icon' => 'hand-o-right',
						'submenu' => [
							[
								'text' => 'Referensi Guru',
								'url'  => 'guru',
								'icon' => 'graduation-cap',
							],
							[
								'text' => 'Referensi Tendik',
								'url'  => 'tendik',
								'icon' => 'graduation-cap',
							],
							[
								'text' => 'Referensi Instruktur',
								'url'  => 'instruktur',
								'icon' => 'graduation-cap',
							],
							[
								'text' => 'Referensi Asesor',
								'url'  => 'asesor',
								'icon' => 'graduation-cap',
							],
						],
					],
					[
						'text' => 'Referensi Rombel',
						'url'  => 'rombel',
						'icon' => 'hand-o-right',
						'permission'	=> 'admin',
					],
					[
						'text' => 'Referensi Peserta Didik',
						'url'  => '#',
						'icon' => 'hand-o-right',
						'submenu' => [
							[
								'text' => 'Peserta Didik Aktif',
								'url'  => 'pd-aktif',
								'icon' => 'users',
							],
							[
								'text' => 'Peserta Didik Keluar',
								'url'  => 'pd-keluar',
								'icon' => 'users',
								'icon_color'	=> 'red',
							],
						],
					],
					[
						'text' => 'Referensi Mata Pelajaran',
						'url'  => 'referensi/mata-pelajaran',
						'icon' => 'hand-o-right',
						'permission'	=> 'admin',
					],
					[
						'text' => 'Referensi Ekstrakurikuler',
						'url'  => 'referensi/ekskul',
						'icon' => 'hand-o-right',
						'permission'	=> 'admin',
					],
					[
						'text' => 'Referensi Teknik Penilaian',
						'url'  => 'referensi/metode',
						'icon' => 'hand-o-right',
						'permission'	=> 'admin',
					],
					[
						'text' => 'Referensi Acuan Sikap',
						'url'  => 'referensi/sikap',
						'icon' => 'hand-o-right',
						'permission'	=> 'admin',
					],
					[
						'text' => 'Referensi Kompetensi Dasar',
						'url'  => 'referensi/kd',
						'icon' => 'hand-o-right',
						'permission'	=> 'guru',
					],
					[
						'text' => 'Referensi Uji Kompetensi',
						'url'  => 'referensi/ukk',
						'icon' => 'hand-o-right',
						'permission'	=> 'wakakur',
					],
				],
			]);
			$event->menu->add([
				'text'	=> 'Perencanaan',
				'url'  => '#',
				'icon' => 'check-square-o',
				'permission'  => 'guru',
				'submenu' => [
					[
						'text' => 'Rasio Nilai Akhir',
						'url'  => 'perencanaan/rasio',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Penilaian Pengetahuan',
						'url'  => 'perencanaan/pengetahuan',
						'icon' => 'hand-o-right',
						'active' => ['perencanaan/pengetahuan', 'perencanaan/tambah-pengetahuan'],
					],
					[
						'text' => 'Penilaian Keterampilan',
						'url'  => 'perencanaan/keterampilan',
						'icon' => 'hand-o-right',
						'active' => ['perencanaan/keterampilan', 'perencanaan/tambah-keterampilan'],
					],
					[
						'text' => 'Bobot Keterampilan',
						'url'  => 'perencanaan/bobot',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Penilaian UKK',
						'url'  => 'perencanaan/ukk',
						'icon' => 'hand-o-right',
						'active' => ['perencanaan/ukk', 'perencanaan/tambah-ukk'],
						'permission'	=> 'internal',
					],
				],
			]);
			$event->menu->add([
				'text'	=> 'Penilaian',
				'url'  => '#',
				'icon' => 'edit',
				'permission'  => 'guru',
				'submenu' => [
					[
						'text' => 'Penilaian Pengetahuan',
						'url'  => 'penilaian/pengetahuan',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Penilaian Keterampilan',
						'url'  => 'penilaian/keterampilan',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Penilaian Sikap',
						'url'  => 'penilaian/list-sikap',
						'icon' => 'hand-o-right',
						'active' => ['penilaian/list-sikap', 'penilaian/sikap'],
					],
					[
						'text' => 'Penilaian Remedial',
						'url'  => 'penilaian/remedial',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Penilaian UKK',
						'url'  => 'penilaian/ukk',
						'icon' => 'hand-o-right',
						'permission'  => 'internal',
					],
				],
			]);
			$event->menu->add([
				'text'	=> 'Monitoring Dan Analisis',
				'url'  => '#',
				'icon' => 'eye',
				'permission'  => 'wali',
				'submenu' => [
					[
						'text' => 'Rekap Nilai',
						'url'  => 'monitoring/rekap-nilai',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Analisi Hasil Penilaian',
						'url'  => 'monitoring/analisis-nilai',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Analisi Hasil Remedial',
						'url'  => 'monitoring/analisis-remedial',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Pencapaian Kompetensi',
						'url'  => 'monitoring/capaian-kompetensi',
						'icon' => 'hand-o-right',
					],
					[
						'text' => 'Prestasi Individu Peserta Didik',
						'url'  => 'monitoring/prestasi-individu',
						'icon' => 'hand-o-right',
					],
				],
			]);
			$event->menu->add([
				'text' => 'Profile',
				'url'  => 'users/profile',
				'icon' => 'user',
				'permission'  => ['superadmin', 'admin', 'guru', 'siswa'],
				'active' => ['users/profile'],
			]);
			$event->menu->add([
				'text' => 'Daftar Perubahan',
				'url'  => 'changelog',
				'icon' => 'check-square-o',
				'permission'  => 'admin',
			]);
			$event->menu->add([
				'text' => 'Cek Pembaharuan',
				'url'  => 'check-update',
				'icon' => 'refresh',
				'permission'  => 'admin',
			]);
			$event->menu->add([
				'text'    => 'User Level',
				'icon'    => 'share',
				'permission'  => 'superadmin',
				'submenu' => [
					[
						'text' => 'Role',
						'url'  => 'role_index',
						'icon' => 'check',
						'active' => ['role_index', 'role_create'],
					],
					[
						'text' => 'Permission',
						'url'  => 'permission',
						'icon' => 'check',
						'active' => ['permission', 'permission_create'],
					],
					[
						'text' => 'Pengguna',
						'url'  => 'pengguna',
						'icon' => 'check',
						'active' => ['pengguna', 'tambah_pengguna'],
					],
				],
			]);
		});
	}
}
