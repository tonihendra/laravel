<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Guru;
use App\Sekolah;
use Illuminate\Support\Facades\DB;
use App\Pembelajaran;
use App\Rombongan_belajar;
use Alert;
use App\NilaiAkhirKeterampilan;
use App\NilaiAkhirPengetahuan;
use App\Nilai_akhir;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
		$user = auth()->user();
		$semester = DB::table('semester')->where('periode_aktif', 1)->first();
		$pembelajaran = '';
		$rombongan_belajar = '';
		if($user->hasRole('guru')){
			$pembelajaran = Pembelajaran::with('mata_pelajaran')->with(['rombongan_belajar' => function($query){
				$query->withCount('anggota_rombel')->with('wali')->orderBy('tingkat', 'asc');
			}])
			->withCount('rencana_pengetahuan')
			->withCount('rencana_keterampilan')
			->withCount('nilai_akhir_pengetahuan')
			->withCount('nilai_akhir_keterampilan')
			->where('sekolah_id', '=', $user->sekolah_id)
			->where('semester_id', '=', $semester->semester_id)
			->where('guru_id', '=', $user->guru_id)
			->whereNotNull('kelompok_id')
			->whereNotNull('no_urut')
			->orWhere('guru_pengajar_id', '=', $user->guru_id)
			->where('sekolah_id', '=', $user->sekolah_id)
			->where('semester_id', '=', $semester->semester_id)
			->whereNotNull('kelompok_id')
			->whereNotNull('no_urut')
			->orderBy('mata_pelajaran_id', 'asc')
			->get();
		}
		if($user->hasRole('wali')){
			$rombongan_belajar = Rombongan_belajar::with(['pembelajaran' => function($query){
				$query->with('guru');
				$query->with('pengajar');
				$query->withCount('rencana_pengetahuan');
				$query->withCount('rencana_keterampilan');
				$query->withCount('nilai_akhir_pengetahuan');
				$query->withCount('nilai_akhir_keterampilan');
				$query->whereNotNull('kelompok_id');
				$query->whereNotNull('no_urut');
			}])->where('sekolah_id', '=', $user->sekolah_id)
			->where('semester_id', '=', $semester->semester_id)
			->where('guru_id', '=', $user->guru_id)->first();
		}
		$params = array(
			'user' 				=> $user,
			'sekolah' 			=> Sekolah::find($user->sekolah_id),
			'guru' 				=> Guru::where('sekolah_id', '=', $user->sekolah_id)->whereNotNull('guru_id_dapodik')->count(),
			'siswa' 			=> DB::table('anggota_rombel')->join('rombongan_belajar', function ($join) {
				$join->on('anggota_rombel.rombongan_belajar_id', '=', 'rombongan_belajar.rombongan_belajar_id')->where('rombongan_belajar.jenis_rombel', '=', 1);
			})->where('anggota_rombel.sekolah_id', '=', $user->sekolah_id)->where('anggota_rombel.semester_id', '=', $semester->semester_id)->count(),
			'semester'			=> $semester,
			'all_pembelajaran'	=> $pembelajaran,
			'rombongan_belajar'	=> $rombongan_belajar,
		);
        return view('home')->with($params);
    }
	public function proses(Request $request){
        $this->validate($request,[
           'name' => 'required|min:1|max:100',
           'description' => 'required',
        ]);
		$role = Role::create([
            'name' => $request['name'],
            'display_name' => $request['name'],
            'description' => $request['description'],
        ]);
		return redirect()->route('home')->with('success', "The role <strong>$role->name</strong> has successfully been created.");
    }
	public function kunci_nilai($rombongan_belajar_id, $status){
		$rombel = Rombongan_belajar::find($rombongan_belajar_id);
		$rombel->kunci_nilai = ($status) ? 0 : 1;
		if($rombel->save()){
			$with = 'success';
			$text = ($status) ? 'berhasil di aktifkan' : 'berhasil di nonaktifkan';
		} else {
			$text = ($status) ? 'gagal di aktifkan' : 'gagal di nonaktifkan';
			$with = 'error';
		}
		return redirect()->route('home')->with($with, 'Status Penilaian di Rombongan Belajar '.$rombel->nama.' '. $text);
	}
	public function generate_nilai($pembelajaran_id, $kompetensi_id){
		$user = auth()->user();
		$rencana_penilaian = ($kompetensi_id == 1) ? 'rencana_pengetahuan' : 'rencana_keterampilan';
		$pembelajaran = Pembelajaran::with([$rencana_penilaian, $rencana_penilaian.'.kd_nilai', $rencana_penilaian.'.kd_nilai.nilai'])->find($pembelajaran_id);
		$result = array();
		foreach($pembelajaran->{$rencana_penilaian} as $rencana){
			foreach($rencana->kd_nilai as $kd_nilai){
				foreach($kd_nilai->nilai as $nilai){
					$record = array();
					$record['pembelajaran_id'] = $pembelajaran->pembelajaran_id;
					$record['anggota_rombel_id'] = $nilai->anggota_rombel_id;
					$record['kompetensi_id'] = $rencana->kompetensi_id;
					$result[$nilai->anggota_rombel_id][$kd_nilai->kd_id] = $record;
				}
			}
		}
		$a=0;
		$b=0;
		foreach($result as $key => $value){
			foreach($value as $k=>$v){
				$pembelajaran_id = $v['pembelajaran_id'];
			}
			if($kompetensi_id == 1){
				$query = NilaiAkhirPengetahuan::where('pembelajaran_id', '=', $pembelajaran_id)->where('anggota_rombel_id', '=', $key)->where('kompetensi_id', '=', $kompetensi_id)->first();
				} else {
				$query = NilaiAkhirKeterampilan::where('pembelajaran_id', '=', $pembelajaran_id)->where('anggota_rombel_id', '=', $key)->where('kompetensi_id', '=', $kompetensi_id)->first();
			}
			if($query->nilai_akhir){
				$find_nilai_akhir = Nilai_akhir::where('pembelajaran_id', '=', $pembelajaran_id)->where('anggota_rombel_id', '=', $key)->where('kompetensi_id', '=', $kompetensi_id)->first();
				if($find_nilai_akhir){
					$a++;
					$find_nilai_akhir->nilai = $query->nilai_akhir;
					$find_nilai_akhir->last_sync = date('Y-m-d H:i:s');
					
				} else {
					$b++;
					$insert_nilai_akhir = array(
						'sekolah_id'		=> $user->sekolah_id,
						'pembelajaran_id'	=> $pembelajaran_id,
						'anggota_rombel_id'	=> $key,
						'kompetensi_id'		=> $kompetensi_id,
						'nilai'				=> $query->nilai_akhir,
						'last_sync'		=> date('Y-m-d H:i:s'),
					);
					Nilai_akhir::create($insert_nilai_akhir);
				}
			}
		}
		$status['icon'] = 'success';
		$status['text'] = "$b siswa berhasil disimpan. $a siswa berhasil diperbaharui";
		$status['insert'] = $b;
		$status['update'] = $a;
		$status['title'] = 'Generate Nilai Selesai!';
		echo json_encode($status);
	}
}
