<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mata_pelajaran_kurikulum;
use App\Providers\HelperServiceProvider;
use App\Rombongan_belajar;
use Yajra\Datatables\Datatables;
use App\Ekstrakurikuler;
use Illuminate\Support\Facades\Storage;
use App\Teknik_penilaian;
use App\Sikap;
use App\Kompetensi_dasar;
use App\Paket_ukk;
use App\Pembelajaran;
use Illuminate\Support\Str;
class ReferensiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
		return view('referensi.list_mata_pelajaran');
    }
	public function list_mata_pelajaran(){
		$user = auth()->user();
		$query = Mata_pelajaran_kurikulum::with('mata_pelajaran')->with('kurikulum')->whereIn('mata_pelajaran_kurikulum.kurikulum_id', function($query){
			$user = auth()->user();
			$semester = HelperServiceProvider::get_ta();
			$query->select('kurikulum_id')
			->from(with(new Rombongan_belajar)->getTable())
			->where('sekolah_id', '=', $user->sekolah_id)
			->where('semester_id', '=', $semester->semester_id);
		})->orderBy('mata_pelajaran_kurikulum.kurikulum_id')->orderBy('mata_pelajaran_id')->orderBy('tingkat_pendidikan_id');
		return Datatables::of($query)->make(true);
	}
	public function ekskul(){
		Storage::disk('public')->delete('anggota_ekskul_by_rombel.json');
		return view('referensi.list_ekskul');
	}
	public function list_ekskul(){
		$user = auth()->user();
		$semester = HelperServiceProvider::get_ta();
		$query = Ekstrakurikuler::with('guru')->with(['rombongan_belajar' => function($query){
			$user = auth()->user();
			$semester = HelperServiceProvider::get_ta();
			$query->where('rombongan_belajar.sekolah_id', '=', $user->sekolah_id)
			->where('rombongan_belajar.semester_id', '=', $semester->semester_id);
		}])
		->where('ekstrakurikuler.sekolah_id', '=', $user->sekolah_id)
		->where('ekstrakurikuler.semester_id', '=', $semester->semester_id);
		return Datatables::of($query)
		->addColumn('anggota', function ($item) {
			$return  = '<div class="text-center"><a href="'.url('rombel/anggota/'.$item->rombongan_belajar->rombongan_belajar_id).'" class="btn btn-primary btn-sm toggle-modal"><i class="fa fa-eye"></i> Anggota Ekskul</a></div>';
			return $return;
		})
		->addColumn('sync_anggota', function ($item) {
			$return  = '<div class="text-center"><a href="'.url('sinkronisasi/anggota-by-rombel/'.$item->rombongan_belajar->rombel_id_dapodik).'" class="btn btn-danger btn-sm"><i class="fa fa-refresh"></i> Sync Anggota</a></div>';
			return $return;
		})
		 ->rawColumns(['anggota', 'sync_anggota'])
		->make(true);
	}
	public function metode(){
		$user = auth()->user();
		$query = Teknik_penilaian::where('sekolah_id', '=', $user->sekolah_id);
		if(!$query->count()){
			$insert_teknik = array(
				array(
					'sekolah_id' 	=> $user->sekolah_id,
					'kompetensi_id'	=> 1,
					'nama'			=> 'Tes Tertulis',
					'last_sync'		=> date('Y-m-d H:i:s'),
				),
				array(
					'sekolah_id' 	=> $user->sekolah_id,
					'kompetensi_id'	=> 1,
					'nama'			=> 'Tes Lisan',
					'last_sync'		=> date('Y-m-d H:i:s'),
				),
				array(
					'sekolah_id' 	=> $user->sekolah_id,
					'kompetensi_id'	=> 1,
					'nama'			=> 'Penugasan',
					'last_sync'		=> date('Y-m-d H:i:s'),
				),
				array(
					'sekolah_id' 	=> $user->sekolah_id,
					'kompetensi_id'	=> 2,
					'nama'			=> 'Portofolio',
					'last_sync'		=> date('Y-m-d H:i:s'),
				),
				array(
					'sekolah_id' 	=> $user->sekolah_id,
					'kompetensi_id'	=> 2,
					'nama'			=> 'Kinerja',
					'last_sync'		=> date('Y-m-d H:i:s'),
				),
				array(
					'sekolah_id' 	=> $user->sekolah_id,
					'kompetensi_id'	=> 2,
					'nama'			=> 'Proyek',
					'last_sync'		=> date('Y-m-d H:i:s'),
				),
			);
			foreach($insert_teknik as $teknik){
				Teknik_penilaian::create($teknik);
			}
			return redirect(url('referensi/metode'));
		}
		return view('referensi.list_metode');
    }
	public function list_metode(){
		$user = auth()->user();
		$query = Teknik_penilaian::where('sekolah_id', '=', $user->sekolah_id);
		return Datatables::of($query)
		->addColumn('kompetensi', function ($item) {
			$return  = ($item->kompetensi_id == 1) ? 'Pengetahuan' : 'Keterampilan';
			return $return;
		})
		->addColumn('tindakan', function ($item) {
			$return  = '-';//'<div class="text-center"><a href="'.url('sinkronisasi/anggota-by-rombel/'.$item->teknik_penilaian_id).'" class="btn btn-danger btn-sm"><i class="fa fa-refresh"></i> Sync Anggota</a></div>';
			return $return;
		})
		 ->rawColumns(['kompetensi', 'tindakan'])
		->make(true);
	}
	public function sikap(){
		$user = auth()->user();
		$query = Sikap::first();
		if(!$query){
			$insert_sikap = array(
				array(
					'butir_sikap'	=> 'Integritas',
					'last_sync'		=> date('Y-m-d H:i:s'),
					'sub_sikap'		=> array(
						array(
							'butir_sikap'	=> 'Kesetiaan',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Antikorupsi',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Keteladanan',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Keadilan',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Menghargai martabat manusia',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
					),
				),
				array(
					'butir_sikap'	=> 'Religius',
					'last_sync'		=> date('Y-m-d H:i:s'),
					'sub_sikap'		=> array(
						array(
							'butir_sikap'	=> 'Melindungi yang kecil dan tersisih',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Taat beribadah',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Menjalankan ajaran agama',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Menjauhi larangan agama',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
					),
				),
				array(
					'butir_sikap'	=> 'Nasionalis',
					'last_sync'		=> date('Y-m-d H:i:s'),
					'sub_sikap'		=> array(
						array(
							'butir_sikap'	=> 'Rela berkorban',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Taat hukum',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Unggul',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Disiplin',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Berprestasi',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Cinta damai',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
					),
				),
				array(
					'butir_sikap'	=> 'Mandiri',
					'last_sync'		=> date('Y-m-d H:i:s'),
					'sub_sikap'		=> array(
						array(
							'butir_sikap'	=> 'Tangguh',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Kerja keras',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Kreatif',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Keberanian',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Pembelajar',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Daya juang',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Berwawasan informasi dan teknologi',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
					),
				),
				array(
					'butir_sikap'	=> 'Gotong-royong',
					'last_sync'		=> date('Y-m-d H:i:s'),
					'sub_sikap'		=> array(
						array(
							'butir_sikap'	=> 'Musyawarah',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Tolong-menolong',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Kerelawanan',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Solidaritas',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
						array(
							'butir_sikap'	=> 'Antidiskriminasi',
							'last_sync'		=> date('Y-m-d H:i:s'),
						),
					),
				),
			);
			foreach($insert_sikap as $sikap){
				//dd($sikap);
				$induk = Sikap::create([
					'butir_sikap'	=> $sikap['butir_sikap'],
					'last_sync'		=> $sikap['last_sync'],
				]);
				foreach($sikap['sub_sikap'] as $sub_sikap){
					Sikap::create([
						'sikap_induk'	=> $induk->sikap_id,
						'butir_sikap'	=> $sub_sikap['butir_sikap'],
						'last_sync'		=> $sub_sikap['last_sync'],
					]);
				}
			}
			return redirect(url('referensi/sikap'));
		}
		$params = array(
			'all_sikap' => $query = Sikap::whereNull('sikap_induk')->with('sikap')->get()
		);
		return view('referensi.list_sikap')->with($params);
    }
	public function kd(){
		$user = auth()->user();
		$semester = HelperServiceProvider::get_ta();
		$params = array(
			'all_pembelajaran' => Pembelajaran::select(['mata_pelajaran_id', 'nama_mata_pelajaran'])->with('mata_pelajaran')->where('sekolah_id', '=', $user->sekolah_id)
			->where('semester_id', '=', $semester->semester_id)
			->where('guru_id', '=', $user->guru_id)
			->orWhere('guru_pengajar_id', '=', $user->guru_id)
			->orderBy('mata_pelajaran_id', 'asc')
			->groupBy('mata_pelajaran_id')
			->groupBy('nama_mata_pelajaran')
			->get(),
		);
		return view('referensi.list_kd')->with($params);
    }
	public function ukk(){
		return view('referensi.list_ukk');
    }
	public function list_kd(Request $request){
		//dd($request);
		$user = auth()->user();
		$semester = HelperServiceProvider::get_ta();
		$user = auth()->user();
		$query = Kompetensi_dasar::with('mata_pelajaran')->whereHas('pembelajaran', function($query) use($user, $semester){
			$query->where('pembelajaran.sekolah_id', '=', $user->sekolah_id)
			->where('pembelajaran.semester_id', '=', $semester->semester_id)
			->where('pembelajaran.guru_id', '=', $user->guru_id)
			->orWhere('pembelajaran.guru_pengajar_id', '=', $user->guru_id);
		})->whereHas('kurikulum')->where('kelas', '!=', 0)->orderBy('ref_kompetensi_dasar.mata_pelajaran_id', 'asc')->orderBy('kelas', 'asc')->get();
		return Datatables::of($query)
		->filter(function ($instance) use ($request) {
			if ($request->has('mata_pelajaran_id')) {
				$instance->collection = $instance->collection->filter(function ($row) use ($request) {
					return Str::contains($row['mata_pelajaran_id'], $request->get('mata_pelajaran_id')) ? true : false;
				});
			}
			if ($request->has('filter_kelas')) {
				$instance->collection = $instance->collection->filter(function ($row) use ($request) {
					return Str::contains($row['kelas'], $request->get('filter_kelas')) ? true : false;
				});
			}
		})
		->addColumn('isi_kd', function ($item) {
			$return  = ($item->kompetensi_dasar_alias) ? $item->kompetensi_dasar_alias : $item->kompetensi_dasar;
			return $return;
		})
		->addColumn('status', function ($item) {
			$return  = HelperServiceProvider::status_label($item->aktif);
			return $return;
		})
		->addColumn('tindakan', function ($item) {
			if($item->aktif){
				$icon_aktif 	= 'fa-close';
				$title_aktif	= 'Non Aktifkan';
			} else {
            	$icon_aktif 	= 'fa-check';
				$title_aktif	= 'Aktifkan';
			}
			//dd($item);
			$return  = '<div class="text-center"><div class="btn-group">
							<button type="button" class="btn btn-default btn-sm">Aksi</button>
                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu pull-right text-left" role="menu">
								 <li><a href="'.url('referensi/edit-kd/'.$item->id).'" class="toggle-modal tooltip-left" title="Tambah/Ubah Ringkasan Kompetensi"><i class="fa fa-pencil"></i>Edit</a></li>
								 <li><a href="'.url('admin/referensi/delete-kd/'.$item->id).'" class="confirm tooltip-left" title="Hapus Ringkasan Kompetensi"><i class="fa fa-power-off"></i>Hapus</a></li>
								 <li><a data-status="'.$item->aktif.'" href="'.url('admin/referensi/toggle_aktif/'.$item->id).'" class="confirm_aktif tooltip-left" title="'.$title_aktif.'"><i class="fa '.$icon_aktif.'"></i>'.$title_aktif.'</a></li>
                            </ul>
                        </div></div>';
			return $return;
		})
		->rawColumns(['isi_kd', 'status', 'tindakan'])
		->make(true);
	}
	public function list_ukk(){
		$user = auth()->user();
		$query = Paket_ukk::with('jurusan')->with('unit_ukk');
		return Datatables::of($query)
		->addColumn('nama_jurusan', function ($item) {
			$return  = $item->jurusan->nama_jurusan;
			return $return;
		})
		->addColumn('jumlah_unit', function ($item) {
			$return  = $item->unit->count();
			return $return;
		})
		->addColumn('status', function ($item) {
			$return  = HelperServiceProvider::status_label($item->status);
			return $return;
		})
		->addColumn('tindakan', function ($item) {
			$ganti_status = ($item->status) ? 'Non Aktifkan' : 'Aktifkan';
			$power_status = ($item->status) ? 'fa-power-off' : 'fa-check';
			$return  = '<div class="text-center"><div class="btn-group">
							<button type="button" class="btn btn-default btn-sm">Aksi</button>
                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu pull-right text-left" role="menu">
								<li><a href="'.url('referensi/tambah-unit-ukk/'.$item->paket_ukk_id).'"><i class="fa fa-plus"></i> Tambah Unit</a></li>
								<li><a href="'.url('referensi/detil-unit-ukk/'.$item->paket_ukk_id).'"><i class="fa fa-search"></i> Detil Unit</a></li>
								<li><a href="'.url('referensi/status-ukk/'.$item->paket_ukk_id.'/'.$item->status).'"><i class="fa '.$power_status.'"></i>'.$ganti_status.'</a></li>
								<li><a href="'.url('referensi/edit-paket-ukk/'.$item->paket_ukk_id).'" class="toggle-modal"><i class="fa fa-pencil"></i>Edit</a></li>
                            </ul>
                        </div></div>';
			return $return;
		})
		->rawColumns(['jumlah_unit', 'status', 'tindakan'])
		->make(true);
	}
}
