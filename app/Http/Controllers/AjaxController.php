<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rombongan_belajar;
use App\Pembelajaran;
use App\Siswa;
use App\Providers\HelperServiceProvider;
use App\Teknik_penilaian;
use App\Kompetensi_dasar;
use App\Bobot_keterampilan;
use App\Rencana_penilaian;
use App\Kd_nilai;
use App\Anggota_rombel;
use App\Sikap;
use App\Nilai_sikap;
class AjaxController extends Controller
{
    public function get_rombel(Request $request){
		$user = auth()->user();
		$query = $request['query'];
		$tingkat = $request['kelas'];
		$semester_id = $request['semester_id'];
		$guru_id = $request['semester_d'];
		if($user->hasRole('guru')){
			$data_rombel = Rombongan_belajar::whereHas('pembelajaran', function($query) use ($user, $semester_id){
				//$user = auth()->user();
				//$semester = HelperServiceProvider::get_ta();
				$query->where('pembelajaran.sekolah_id', '=', $user->sekolah_id);
				$query->where('pembelajaran.semester_id', '=', $semester_id);
				$query->where('pembelajaran.guru_id', '=', $user->guru_id);
				$query->whereNotNull('kelompok_id');
				$query->whereNotNull('no_urut');
				$query->orWhere('pembelajaran.sekolah_id', '=', $user->sekolah_id);
				$query->where('pembelajaran.semester_id', '=', $semester_id);
				$query->where('pembelajaran.guru_pengajar_id', '=', $user->guru_id);
				$query->whereNotNull('kelompok_id');
				$query->whereNotNull('no_urut');
			})
			->where('tingkat', '=', $tingkat)
			->where('semester_id', '=', $semester_id)
			->orderBy('nama')
			->orderBy('tingkat')
			->get();
		} else {
			$data_rombel = Rombongan_belajar::where('sekolah_id', '=', $user->sekolah_id)->where('tingkat', '=', $tingkat)->where('semester_id', '=', $semester_id)->orderBy('nama')->orderBy('tingkat')->get();
		}
		if($data_rombel->count()){
			foreach($data_rombel as $rombel){
				$record= array();
				$record['value'] 	= $rombel->rombongan_belajar_id;
				$record['text'] 	= $rombel->nama;
				$output['result'][] = $record;
			}
		} else {
			$record['value'] 	= '';
			$record['text'] 	= 'Tidak ditemukan rombongan belajar di kelas terpilih';
			$output['result'][] = $record;
		}
		echo json_encode($output);
	}
	public function get_mapel(Request $request){
		$user = auth()->user();
		$rombongan_belajar_id = $request['rombel_id'];
		$semester_id = $request['semester_id'];
		$all_mapel = Pembelajaran::where('rombongan_belajar_id', '=', $rombongan_belajar_id)
		->where('guru_id', '=', $user->guru_id)
		->whereNotNull('kelompok_id')
		->whereNotNull('no_urut')
		->orWhere('rombongan_belajar_id', '=', $rombongan_belajar_id)
		->where('guru_pengajar_id', '=', $user->guru_id)
		->whereNotNull('kelompok_id')
		->whereNotNull('no_urut')
		->orderBy('mata_pelajaran_id', 'asc')
		->get();
		if($all_mapel->count()){
			foreach($all_mapel as $mapel){
				$record= array();
				$record['value'] 	= $mapel->mata_pelajaran_id;
				$record['text'] 	= $mapel->nama_mata_pelajaran.' ('.$mapel->mata_pelajaran_id.')';
				$record['pembelajaran_id'] 	= $mapel->pembelajaran_id;
				$output['mapel'][] = $record;
			}
		} else {
			$record['value'] 	= '';
			$record['text'] 	= 'Tidak ditemukan mata pelajaran di kelas terpilih';
			$record['pembelajaran_id'] 	= '';
			$output['mapel'][] = $record;
		}
		echo json_encode($output);
	}
	public function get_teknik(Request $request){
		$user = auth()->user();
		$kompetensi_id = $request['kompetensi_id'];
		$all_bentuk_penilaian = Teknik_penilaian::where('sekolah_id', '=', $user->sekolah_id)->where('kompetensi_id', '=', $kompetensi_id)->get();
		if($all_bentuk_penilaian->count()){
			foreach($all_bentuk_penilaian as $bentuk_penilaian){
				$record= array();
				$record['value'] 	= $bentuk_penilaian->teknik_penilaian_id;
				$record['text'] 	= $bentuk_penilaian->nama;
				$output['result'][] = $record;
			}
		} else {
			$record['value'] 	= '';
			$record['text'] 	= 'Tidak ditemukan teknik penilaian keterampilan';
			$output['result'][] = $record;
		}
		echo json_encode($output);
	}
	public function get_kd(Request $request){
		$user = auth()->user();
		$kompetensi_id = $request['kompetensi_id'];
		$aspek = ($kompetensi_id == 1) ? 'P' : 'K';
		$id_mapel = $request['id_mapel'];
		$kelas = $request['kelas'];
		$id_rombel = $request['rombel_id'];
		$metode_id = $request['teknik_penilaian'];
		$rombongan_belajar = Rombongan_belajar::find($id_rombel);
		$all_kd = Kompetensi_dasar::where('aspek', '=', $aspek)->where('mata_pelajaran_id', '=', $id_mapel)->where('kelas', '=', $kelas)->where('aktif', '=', 1)->where('kurikulum_id', '=', $rombongan_belajar->kurikulum_id)->get();
		$bobot = '';
		$bentuk_penilaian = '';
		if($kompetensi_id == 1){
			$bentuk_penilaian = Teknik_penilaian::where('sekolah_id', '=', $user->sekolah_id)->where('kompetensi_id', '=', $kompetensi_id)->get();
		} else {
			$pembelajaran = Pembelajaran::where('rombongan_belajar_id', '=', $rombongan_belajar->rombongan_belajar_id)
			->where('mata_pelajaran_id', '=', $id_mapel)
			->first();
			if($pembelajaran){
				$find_bobot_keterampilan = Bobot_keterampilan::where('pembelajaran_id', '=', $pembelajaran->pembelajaran_id)->where('metode_id', '=', $metode_id)->first();
				if($find_bobot_keterampilan){
					$bobot = $find_bobot_keterampilan->bobot;
				}
			}
		}
		$params = array(
			'all_kd' => $all_kd,
			'bobot' => $bobot,
			'kompetensi_id' => $kompetensi_id,
			'id_rombel'	=> $id_rombel,
			'id_mapel'	=> $id_mapel,
			'kelas'	=> $kelas,
			'placeholder' => ($kompetensi_id == 1) ? 'UH/PTS/PAS dll...' : 'Kinerja/Proyek/Portofolio',
			'bentuk_penilaian' => $bentuk_penilaian,
		);
		return view('perencanaan.get_kd_'.$kompetensi_id)->with($params);
	}
	public function get_bobot($pembelajaran_id, $metode_id){
		$find_bobot = Bobot_keterampilan::where('pembelajaran_id', '=', $pembelajaran_id)->where('metode_id', '=', $metode_id)->first();
		if($find_bobot){
			echo $find_bobot->bobot;
		}
	}
	public function get_rencana(Request $request){
		$user = auth()->user();
		$pembelajaran_id = $request['pembelajaran_id'];
		$kompetensi_id = $request['kompetensi_id'];
		$get_rencana = Rencana_penilaian::where('pembelajaran_id', '=', $pembelajaran_id)->where('kompetensi_id', '=', $kompetensi_id)->get();
		if($get_rencana->count()){
			foreach($get_rencana as $rencana){
				$record= array();
				$record['value'] 	= $rencana->rencana_penilaian_id;
				$record['text'] 	= $rencana->nama_penilaian;
				$output['result'][] = $record;
			}
		} else {
			$record['value'] 	= '';
			$record['text'] 	= 'Tidak ditemukan rencana penilaian di mata pelajaran terpilih';
			$output['result'][] = $record;
		}
		echo json_encode($output);
	}
	public function get_kompetensi(Request $request){
		$get_kompetensi = array(
			array('id' => 1, 'nama' => 'Pengetahuan'),
			array('id' => 2, 'nama' => 'Keterampilan'),
		);
		foreach($get_kompetensi as $kompetensi){
			$record= array();
			$record['value'] 	= $kompetensi['id'];
			$record['text'] 	= $kompetensi['nama'];
			$output['aspek_penilaian'][] = $record;
		}
		echo json_encode($output);
	}
	public function get_remedial(Request $request){
		$get_kompetensi = array(
			array('id' => 1, 'nama' => 'Pengetahuan'),
			array('id' => 2, 'nama' => 'Keterampilan'),
		);
		foreach($get_kompetensi as $kompetensi){
			$record= array();
			$record['value'] 	= $kompetensi['id'];
			$record['text'] 	= $kompetensi['nama'];
			$output['aspek_penilaian'][] = $record;
		}
		echo json_encode($output);
	}
	public function get_siswa(Request $request){
		$rombongan_belajar_id = $request['rombel_id'];
		$get_siswa = Anggota_rombel::with('siswa')->where('rombongan_belajar_id', '=', $rombongan_belajar_id)->get();
		if($get_siswa->count()){
			foreach($get_siswa as $siswa){
				$record= array();
				$record['value'] 	= $siswa->anggota_rombel_id;
				$record['text'] 	= strtoupper($siswa->siswa->nama);
				$output['siswa'][] = $record;
			}
		} else {
			$record['value'] 	= '';
			$record['text'] 	= 'Tidak ditemukan rencana penilaian di mata pelajaran terpilih';
			$output['siswa'][] = $record;
		}
		echo json_encode($output);
	}
	public function get_sikap(Request $request){
		$anggota_rombel_id = $request['siswa_id'];
		$guru_id = $request['guru_id'];
		$nilai_sikap = Nilai_sikap::with('ref_sikap')->where('anggota_rombel_id', '=', $anggota_rombel_id)->where('guru_id', '=', $guru_id)->get();
		$params = array(
			'nilai_sikap' => $nilai_sikap,
			'all_sikap' => $query = Sikap::whereNull('sikap_induk')->with('sikap')->get(),
			'guru_id'	=> $guru_id,
		);
		return view('penilaian.penilaian_sikap')->with($params);
	}
	public function get_kd_nilai(Request $request){
		$user = auth()->user();
		$kompetensi_id = $request['kompetensi_id'];
		$rencana_penilaian_id = $request['rencana_id'];
		$rombongan_belajar_id = $request['rombel_id'];
		$pembelajaran_id = $request['pembelajaran_id'];
		$rencana_penilaian_id = $request['rencana_id'];
		$all_kd_nilai = Rencana_penilaian::with(['kd_nilai','kd_nilai.kompetensi_dasar'])->find($rencana_penilaian_id);
		//Kd_nilai::where('rencana_penilaian_id', '=', $rencana_penilaian_id)->get();
		$all_anggota = Anggota_rombel::with('siswa')->where('rombongan_belajar_id', '=', $rombongan_belajar_id)->get();
		$all_bobot = Rencana_penilaian::where('pembelajaran_id', '=', $pembelajaran_id)->where('kompetensi_id', '=', $kompetensi_id)->sum('bobot');
		$params = array(
			'all_kd_nilai' 			=> $all_kd_nilai,
			'all_anggota' 			=> $all_anggota,
			'all_bobot'				=> $all_bobot,
			'kompetensi_id'			=> $kompetensi_id,
			'bobot'					=> $all_kd_nilai->bobot,
			'rencana_penilaian_id'	=> $rencana_penilaian_id,
		);
		return view('penilaian.get_kd_nilai')->with($params);
	}
	public function get_rekap_nilai(Request $request){
		$pembelajaran_id = $request['pembelajaran_id'];
		$pembelajaran = Pembelajaran::with(['anggota_rombel', 'anggota_rombel.siswa', 'anggota_rombel.nilai_akhir_pengetahuan' => function($query) use ($pembelajaran_id){
			$query->where('pembelajaran_id', '=', $pembelajaran_id);
		}, 'anggota_rombel.nilai_akhir_keterampilan' => function($query) use ($pembelajaran_id){
			$query->where('pembelajaran_id', '=', $pembelajaran_id);
		}])->find($pembelajaran_id);
		$params['pembelajaran'] = $pembelajaran;
		$params['rasio_p'] = ($pembelajaran->rasio_p) ? $pembelajaran->rasio_p : 50;
		$params['rasio_k'] = ($pembelajaran->rasio_k) ? $pembelajaran->rasio_k : 50;
		return view('monitoring.result_rekap_nilai')->with($params);
	}
	public function get_analisis_nilai(Request $request){
		$kompetensi_id = $request['kompetensi_id'];
		$nilai_kd = ($kompetensi_id == 1) ? 'pembelajaran.anggota_rombel.nilai_kd_pengetahuan' : 'pembelajaran.anggota_rombel.nilai_kd_keterampilan';
		$rencana_penilaian = Rencana_penilaian::with(['pembelajaran', 'pembelajaran.rombongan_belajar', 'pembelajaran.anggota_rombel', $nilai_kd])->find($request['rencana_penilaian']);
		$params = array(
			'kompetensi_id'		=> $kompetensi_id,
			'rencana_penilaian'	=> $rencana_penilaian,
		);
		return view('monitoring.result_analisis_nilai')->with($params);
	}
}
