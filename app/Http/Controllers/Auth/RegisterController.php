<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Ixudra\Curl\Facades\Curl;
use App\Sekolah;
use App\Role;
use App\Role_user;
use Illuminate\Support\Facades\DB;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
		$data_sync = array(
			'username_dapo'	=> $data['email'],
			'password_dapo'	=> $data['password'],
			'npsn'			=> $data['name'],
		);
		$host_server = 'http://103.40.55.242/erapor_server/sync/register';
		$response = Curl::to($host_server)
        ->withData($data_sync)
        ->post();
		$response = json_decode($response);
		if($response){
			$set_data = $response->data;
			$data_sekolah = array(
				'npsn' 					=> $set_data->npsn,
				'nss' 					=> ($set_data->nss) ? $set_data->nss : 0,
				'nama' 					=> $set_data->nama,
				'alamat' 				=> $set_data->alamat,
				'desa_kelurahan'		=> $set_data->desa_kelurahan,
				'kode_wilayah'			=> $set_data->kode_wilayah,
				'kecamatan' 			=> $set_data->kecamatan,
				'kabupaten' 			=> $set_data->kabupaten,
				'provinsi' 				=> $set_data->provinsi,
				'kode_pos' 				=> $set_data->kode_pos,
				'lintang' 				=> $set_data->lintang,
				'bujur' 				=> $set_data->bujur,
				'no_telp' 				=> $set_data->no_telp,
				'no_fax' 				=> $set_data->no_fax,
				'email' 				=> $set_data->email,
				'website' 				=> $set_data->website,
				'status_sekolah'		=> 0,
				'last_sync'				=> date('Y-m-d H:i:s'),
			);
			$sekolah = Sekolah::updateOrCreate(
				['sekolah_id' => $set_data->sekolah_id_dapodik],
				$data_sekolah
			);
			$user = User::updateOrCreate(
				['name' => 'Administrator','email' => $data['email']],
				['password' => Hash::make($data['password']), 'last_sync' => date('Y-m-d H:i:s'), 'sekolah_id' => $set_data->sekolah_id_dapodik, 'password_dapo'	=> md5($data['password'])]
			);
			$adminRole = Role::where('name', 'admin')->first();
			$user = User::where('email', $data['email'])->first();
			$CheckadminRole = DB::table('role_user')->where('user_id', $user->user_id)->first();
			if(!$CheckadminRole){
				$user->attachRole($adminRole);
			}
			return $user;
		}
    }
}
