<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    public $incrementing = false;
	protected $table = 'ref_sekolah';
	protected $primaryKey = 'sekolah_id';
	protected $guarded = [];
	/*protected $fillable = [
        'sekolah_id', 'npsn', 'nama', 'status_sekolah', 'last_sync',
    ];*/
}
