<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
class Mou extends Model
{
    use Uuid;
    public $incrementing = false;
	protected $table = 'mou';
	protected $primaryKey = 'mou_id';
	protected $guarded = [];
}
