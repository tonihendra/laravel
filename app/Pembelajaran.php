<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Providers\HelperServiceProvider;
class Pembelajaran extends Model
{
    use Uuid;
    public $incrementing = false;
	protected $table = 'pembelajaran';
	protected $primaryKey = 'pembelajaran_id';
	protected $guarded = [];
	public function guru(){
		return $this->hasOne('App\Guru', 'guru_id', 'guru_id');
	}
	public function pengajar(){
		return $this->hasOne('App\Guru', 'guru_id', 'guru_pengajar_id');
	}
	public function mata_pelajaran(){
		return $this->hasOne('App\Mata_pelajaran', 'mata_pelajaran_id', 'mata_pelajaran_id');
	}
	public function rombongan_belajar(){
		return $this->hasOne('App\Rombongan_belajar', 'rombongan_belajar_id', 'rombongan_belajar_id');
	}
	public function rencana_pengetahuan(){
		return $this->hasMany('App\Rencana_penilaian', 'pembelajaran_id', 'pembelajaran_id')->where('kompetensi_id', '=', 1);
	}
	public function rencana_keterampilan(){
		return $this->hasMany('App\Rencana_penilaian', 'pembelajaran_id', 'pembelajaran_id')->where('kompetensi_id', '=', 2);
	}
	public function nilai_akhir_pengetahuan(){
		return $this->hasMany('App\Nilai_akhir', 'pembelajaran_id', 'pembelajaran_id')->where('kompetensi_id', '=', 1);
	}
	public function nilai_akhir_keterampilan(){
		return $this->hasMany('App\Nilai_akhir', 'pembelajaran_id', 'pembelajaran_id')->where('kompetensi_id', '=', 2);
	}
	public function anggota_rombel(){
        return $this->hasManyThrough(
            'App\Anggota_rombel',
			'App\Rombongan_belajar',
			'rombongan_belajar_id',
			'rombongan_belajar_id',
			'rombongan_belajar_id',
			'rombongan_belajar_id'
        );
    }
}
