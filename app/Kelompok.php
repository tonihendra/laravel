<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelompok extends Model
{
    protected $table = 'ref_kelompok';
	protected $primaryKey = 'kelompok_id';
	protected $guarded = [];
}
