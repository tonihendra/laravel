<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiKeterampilanPerKd extends Model
{
    protected $table = 'view_nilai_keterampilan_perkd';
}
