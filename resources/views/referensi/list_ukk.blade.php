@extends('adminlte::page')

@section('title', 'eRaporSMK')

@section('content_header')
    <h1>Referensi Uji Kompetensi Keahlian (UKK)</h1>
@stop

<?php
/*
@section('box-title')
	Judul
@stop
*/
?>
@section('content')
	<table id="datatable" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th width="25%">Kompetensi Keahlian</th>
				<th width="10%">Nomor Paket</th>
				<th width="50%">Nama Paket</th>
				<th width="5%" class="text-center">Jml Unit</th>
				<th width="5%" class="text-center">Status</th>
				<th width="10%" class="text-center">Aksi</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
<div id="modal_content" class="modal fade"></div>
@stop

@section('js')
<script type="text/javascript">
var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
var bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
function turn_on_icheck(){
	$('a.toggle-modal').bind('click',function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		if (url.indexOf('#') == 0) {
			$('#modal_content').modal('open');
	        $('.editor').wysihtml5();
		} else {
			$.get(url, function(data) {
				$('#modal_content').modal();
				$('#modal_content').html(data);
			});
		}
	});
}
$(document).ready( function () {
	var table = $('#datatable').DataTable( {
		"retrieve": true,
		"processing": true,
        "serverSide": true,
        "ajax": "{{ url('referensi/list-ukk') }}",
		"columns": [
            { "data": "nama_jurusan" },
            { "data": "nomor_paket" },
			{ "data": "nama_paket" },
			{ "data": "jumlah_unit" },
			{ "data": "status" },
			{ "data": "tindakan" },
        ],
		"fnDrawCallback": function(oSettings){
			turn_on_icheck();
		}
    });
});
</script>
@Stop