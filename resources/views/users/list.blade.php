@extends('adminlte::page')

@section('title', 'eRaporSMK')

@section('content_header')
    <h1>Data Pengguna</h1>
@stop

<?php
/*
@section('box-title')
	Judul
@stop
*/
?>
@section('content')
	<table id="datatable" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>Nama</th>
				<th>Email</th>
				<th>Jenis Pengguna</th>
				<th>Terakhir Login</th>
				<th class="text-center">Status Password</th>
				<th class="text-center">Tindakan</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

@section('js')
@include('sweet::alert')
<script type="text/javascript">
$(document).ready( function () {
	$('#datatable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "{{ url('users/list_user') }}",
		"columns": [
			{ "data": "name", "name": 'users.name' },
            //{ "data": "users.name" },
			{ "data": "email", "name": 'users.email' },
			{ "data": "jenis_pengguna" },
			/*{ 
				"data": "roles",
        		"defaultContent": null,
				"searchable": false,
				"orderable": false,
				"render": ( data, type, full ) => {
					console.log(data);
					return $.map( data, function ( d, i ) {
						return d.description;
						}
					).join( ',<br />' );
				}
			},
            { 
				"searchable": false,
				"data": "last_login_at",
				"render": function ( data, type, row, meta ) {
					var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
					var bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
					var tanggal = (data) ? new Date(data).getDate() : null;
					var xhari = (data) ? new Date(data).getDay() : null;
					var xbulan = (data) ? new Date(data).getMonth() : null;
					var xtahun = (data) ? new Date(data).getYear() : null;
					var hari = hari[xhari];
					var bulan = bulan[xbulan];
					var tahun = (xtahun < 1000) ? xtahun + 1900 : xtahun;
					var jam = (data) ? new Date(data).getHours() : null;
					var menit = (data) ? new Date(data).getMinutes() : null;
					var tampil = (data) ? hari +', ' + tanggal + ' ' + bulan + ' ' + tahun + ' '+jam+':'+menit: null;
      				return tampil;
    			}
			},*/
			{ "data": "last_login", "orderable": false, "searchable": false },
            { "data": "hashedPassword", "orderable": false, "searchable": false },
            { "data": "actions", "orderable": false, "searchable": false},
            //{ "data": "last_sync" }
        ]
    } );
});
</script>
@Stop