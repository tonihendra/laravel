<?php $nilai_value = array(); ?>
@foreach($rencana_penilaian->pembelajaran->anggota_rombel as $anggota_rombel)
	<?php $skor_akhir = 0; ?>
	@if($kompetensi_id == 1)
		@foreach($anggota_rombel->nilai_kd_pengetahuan as $kd_pengetahuan)
			<?php $skor_akhir += $kd_pengetahuan->nilai_kd; ?>
		@endforeach
		<?php $result = ($skor_akhir) ? number_format($skor_akhir / count($anggota_rombel->nilai_kd_pengetahuan), 0) : 0; ?>
		<?php $nilai_value[strtoupper($anggota_rombel->siswa->nama)] = $result; ?>
	@else
		@foreach($anggota_rombel->nilai_kd_keterampilan as $kd_keterampilan)
			<?php $skor_akhir += $kd_keterampilan->nilai_kd; ?>
		@endforeach
		<?php $result = ($skor_akhir) ? number_format($skor_akhir / count($anggota_rombel->nilai_kd_keterampilan), 0) : 0; ?>
		<?php $nilai_value[strtoupper($anggota_rombel->siswa->nama)] = $result; ?>
	@endif
@endforeach
<p><strong>Sebaran Hasil Penilaian Per Rencana Penilaian</strong></p>
<div class="row">
	<div class="col-sm-6">
	<table class="table table-bordered table-striped">
		<tr>
			<td width="40%">Rombongan Belajar</td>
			<td class="text-center" width="5%">:</td>
			<td width="55%">{{$rencana_penilaian->pembelajaran->rombongan_belajar->nama}}</td>
		</tr>
		<tr>
			<td>Mata Pelajaran</td>
			<td class="text-center">:</td>
			<td>{{$rencana_penilaian->pembelajaran->nama_mata_pelajaran}}</td>
		</tr>
		<tr>
			<td>Penilaian</td>
			<td class="text-center">:</td>
			<td>{{$rencana_penilaian->nama_penilaian}}</td>
		</tr>
		<tr>
			<td>SKM</td>
			<td class="text-center">:</td>
			<td>{{CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm)}}</td>
		</tr>
		<tr>
			<td>Bobot Penilaian</td>
			<td class="text-center">:</td>
			<td>{{$rencana_penilaian->bobot}}</td>
		</tr>
	</table>
	</div>
</div>
<div class="row">
	<div class="col-sm-9">
		<div id="bar-chart" style="height: 300px;"></div>
	</div>
	<div class="col-sm-3">
		<table class="table table-bordered table-hover">
			<tr>
				<td width="50%" class="text-center"><a class="tooltip-left" href="javascript:void(0)" title="95-100">A+</a></td>
				<td width="50%" class="text-center"><?php echo CustomHelper::sebaran_tooltip($nilai_value,CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'A'),CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'A+'),'left'); ?></td>
			</tr>
			<tr>
				<td class="text-center"><a class="tooltip-left" href="javascript:void(0)" title="90-94">A</a></td>
				<td class="text-center"><?php echo CustomHelper::sebaran_tooltip($nilai_value,CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'A-'),CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'A'),'left'); ?></td>
			</tr>
			<tr>
				<td class="text-center"><a class="tooltip-left" href="javascript:void(0)" title="80-84">B+</a></td>
				<td class="text-center"><?php echo CustomHelper::sebaran_tooltip($nilai_value,CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'B'),CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'B+'),'left'); ?></td>
			</tr>
			<tr>
				<td class="text-center"><a class="tooltip-left" href="javascript:void(0)" title="75-79">B</a></td>
				<td class="text-center"><?php echo CustomHelper::sebaran_tooltip($nilai_value,CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'B-'),CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'B'),'left'); ?></td>
			</tr>
			<tr>
				<td class="text-center"><a class="tooltip-left" href="javascript:void(0)" title="70-74">B-</a></td>
				<td class="text-center"><?php echo CustomHelper::sebaran_tooltip($nilai_value,CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'C'),CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'B-'),'left'); ?></td>
			</tr>
			<tr>
				<td class="text-center"><a class="tooltip-left" href="javascript:void(0)" title="60-69">C</a></td>
				<td class="text-center"><?php echo CustomHelper::sebaran_tooltip($nilai_value,CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'C'),'left'); ?></td>
			</tr>
			<tr>
				<td class="text-center"><a class="tooltip-left" href="javascript:void(0)" title="0-59">D</a></td>
				<td class="text-center"><?php echo CustomHelper::sebaran_tooltip($nilai_value,0,CustomHelper::predikat(CustomHelper::get_kkm($rencana_penilaian->pembelajaran->kelompok_id, $rencana_penilaian->pembelajaran->kkm),'D'),'left'); ?></td>
			</tr>
		</table>
	</div>
</div>
<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/tooltip/tooltip-viewport.css') }}">
<script src="{{ asset('vendor/adminlte/plugins/tooltip/tooltip-viewport.js') }}"></script>
<!-- FLOT CHARTS -->
<script src="{{ asset('vendor/adminlte//plugins/flot/jquery.flot.min.js') }}"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="{{ asset('vendor/adminlte//plugins/flot/jquery.flot.resize.min.js') }}"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="{{ asset('vendor/adminlte//plugins/flot/jquery.flot.pie.min.js') }}"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="{{ asset('vendor/adminlte//plugins/flot/jquery.flot.categories.min.js') }}"></script>
<script>
$(function () {
	/*
	* BAR CHART
	* --------
	*/
	var a1 = [
        [0, {{ count(CustomHelper::sebaran($nilai_value,100,95)) }}],
		[1, {{ count(CustomHelper::sebaran($nilai_value,94,90)) }}],
		[2, {{ count(CustomHelper::sebaran($nilai_value,89,85)) }}],
		[3, {{ count(CustomHelper::sebaran($nilai_value,84,80)) }}],
		[4, {{ count(CustomHelper::sebaran($nilai_value,79,75)) }}],
		[5, {{ count(CustomHelper::sebaran($nilai_value,74,70)) }}],
		[6, {{ count(CustomHelper::sebaran($nilai_value,69,65)) }}],
		[7, {{ count(CustomHelper::sebaran($nilai_value,64,60)) }}],
		[8, {{ count(CustomHelper::sebaran($nilai_value,59,55)) }}],
		[9, {{ count(CustomHelper::sebaran($nilai_value,54,50)) }}],
		[10, {{ count(CustomHelper::sebaran($nilai_value,49,0)) }}]

    ];
	var bar_data = [{
        label: "Sebaran Peserta Didik",
        data: a1,
		color: "#3c8dbc"
    }];
	$.plot("#bar-chart", bar_data, {
		series: {
			bars: {
				show: true,
				barWidth: 0.5,
				align: "center"
				}
		},
		xaxis: {
			mode: "categories",
			tickLength: 0,
			ticks: [
                [0, "100-95"],
                [1, "94-90"],
                [2, "89-85"],
                [3, "84-80"],
				[4, "79-75"],
				[5, "74-70"],
				[6, "69-65"],
				[7, "64-60"],
				[8, "59-55"],
				[9, "54-50"],
				[10, "49-0"],
            ]
		},
		yaxis: {
			tickDecimals:0
		},
		grid: {
            hoverable: true,
            clickable: true,
			borderWidth: 1,
			borderColor: "#f3f3f3",
			tickColor: "#f3f3f3"
        },
		valueLabels: {
            show: true
        },
		legend: {
        show: true,
        noColumns: 0,
		margin: [10,-10]
    	},
	});
                /* END BAR CHART */
});
var previousPoint = null,
    previousLabel = null;

function showTooltip(x, y, color, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 40,
        left: x - 30,
        border: '2px solid ' + color,
        padding: '3px',
            'font-size': '9px',
            'border-radius': '5px',
            'background-color': '#fff',
            'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(200);
}


$("#bar-chart").on("plothover", function (event, pos, item) {
    if (item) {
        if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
            previousPoint = item.dataIndex;
            previousLabel = item.series.label;
            $("#tooltip").remove();

            var x = item.datapoint[0];
            var y = item.datapoint[1];
            var color = item.series.color;

            //console.log(item.series.xaxis.ticks[x].label);               

            showTooltip(item.pageX,
            item.pageY,
            color,
                "<strong>" + item.series.label + "</strong><br>Rentang nilai " + item.series.xaxis.ticks[x].label + " : <strong>" + y + " siswa</strong>");
        }
    } else {
        $("#tooltip").remove();
        previousPoint = null;
    }
});
</script>