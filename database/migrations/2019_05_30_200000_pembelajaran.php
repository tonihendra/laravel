<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pembelajaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelajaran', function (Blueprint $table) {
			$table->uuid('pembelajaran_id');
			$table->uuid('pembelajaran_id_dapodik')->nullable();
			$table->uuid('sekolah_id');
			$table->string('semester_id', 5);
			$table->uuid('rombongan_belajar_id');
			$table->uuid('guru_id')->nullable();
			$table->uuid('guru_pengajar_id')->nullable();
            $table->integer('mata_pelajaran_id');
			$table->string('nama_mata_pelajaran');
			$table->integer('kelompok_id')->nullable();
			$table->integer('no_urut')->nullable();
			$table->integer('kkm')->nullable();
			$table->integer('is_dapodik')->nullable();
			$table->integer('rasio_p')->nullable();
			$table->integer('rasio_k')->nullable();
			$table->integer('pembelajaran_id_erapor')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->foreign('sekolah_id')->references('sekolah_id')->on('ref_sekolah')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('mata_pelajaran_id')->references('mata_pelajaran_id')->on('mata_pelajaran')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('rombongan_belajar_id')->references('rombongan_belajar_id')->on('rombongan_belajar')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('semester_id')->references('semester_id')->on('semester')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('guru_id')->references('guru_id')->on('ref_guru')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('guru_pengajar_id')->references('guru_id')->on('ref_guru')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('kelompok_id')->references('kelompok_id')->on('ref_kelompok')
                ->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->primary('pembelajaran_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelajaran');
    }
}
