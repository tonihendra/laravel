<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NilaiSikap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_sikap', function (Blueprint $table) {
			$table->uuid('nilai_sikap_id');
			$table->uuid('sekolah_id');
			$table->uuid('guru_id');
			$table->uuid('anggota_rombel_id');
			$table->date('tanggal_sikap');
			$table->string('butir_sikap');
			$table->integer('opsi_sikap');
			$table->text('uraian_sikap');
			$table->integer('nilai_sikap_id_erapor')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->timestamp('last_sync');
			$table->primary('nilai_sikap_id');
			$table->foreign('sekolah_id')->references('sekolah_id')->on('ref_sekolah')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('guru_id')->references('guru_id')->on('ref_guru')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('anggota_rombel_id')->references('anggota_rombel_id')->on('anggota_rombel')
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_sikap');
    }
}
