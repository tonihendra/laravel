<?php

use Illuminate\Database\Seeder;

class Mst_wilayahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Level_wilayah::query()->truncate();
		$json = File::get('database/data/level_wilayah.json');
		$data = json_decode($json);
        foreach($data as $obj){
    		DB::table('level_wilayah')->insert([
    			'id_level_wilayah' 	=> $obj->id_level_wilayah,
    			'level_wilayah' 	=> $obj->level_wilayah,
    			'created_at' 		=> $obj->created_at,
				'updated_at' 		=> $obj->updated_at,
				'deleted_at'		=> $obj->deleted_at,
				'last_sync'			=> $obj->last_sync,
    		]);
    	}
		App\Negara::query()->truncate();
		$json = File::get('database/data/negara.json');
		$data = json_decode($json);
		foreach($data as $obj){
    		DB::table('negara')->insert([
    			'negara_id' 		=> $obj->negara_id,
    			'nama' 				=> $obj->nama,
				'luar_negeri'		=> $obj->luar_negeri,
    			'created_at' 		=> $obj->created_at,
				'updated_at' 		=> $obj->updated_at,
				'deleted_at'		=> $obj->deleted_at,
				'last_sync'			=> $obj->last_sync,
    		]);
    	}
		App\Mst_wilayah::query()->truncate();
		$limit = 10000;
		for($i = 0; $i <= 97881; $i += $limit){
			$json = File::get('database/data/mst_wilayah_'.$i.'.json');
			$data = json_decode($json);
			foreach($data as $obj){
				DB::table('mst_wilayah')->insert([
					'kode_wilayah' 		=> $obj->kode_wilayah,
					'nama' 				=> $obj->nama,
					'id_level_wilayah'	=> $obj->id_level_wilayah,
					'mst_kode_wilayah'	=> $obj->mst_kode_wilayah,
					'negara_id'			=> $obj->negara_id,
					'asal_wilayah'		=> $obj->asal_wilayah,
					'kode_bps'			=> $obj->kode_bps,
					'kode_dagri'		=> $obj->kode_dagri,
					'kode_keu'			=> $obj->kode_keu,
					'created_at' 		=> $obj->created_at,
					'updated_at' 		=> $obj->updated_at,
					'deleted_at'		=> $obj->deleted_at,
					'last_sync'			=> $obj->last_sync,
				]);
			}
		}
    }
}
